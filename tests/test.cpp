#define CATCH_CONFIG_MAIN

#include "tcpconnection.h"
#include "catch.hpp"


TEST_CASE("is_filesystem_request", "[function]")
{
    SECTION("Function returns true for correct filesystem request")
    {
        QByteArray message = "FILESYSTEM\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_filesystem_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "FileSystem\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_filesystem_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of filesystem request")
    {
        QByteArray message = "FILESYSTEM";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_filesystem_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_filesystem_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "DELETE\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_filesystem_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_upload_request", "[function]")
{
    SECTION("Function returns true for correct upload request")
    {
        QByteArray message = "UPLOAD\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_upload_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "Upload\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_upload_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of upload request")
    {
        QByteArray message = "UPLOAD";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_upload_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_upload_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "DOWNLOAD\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_upload_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_download_request", "[function]")
{
    SECTION("Function returns true for correct download request")
    {
        QByteArray message = "DOWNLOAD\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_download_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "downLOAD\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_download_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of download request")
    {
        QByteArray message = "DOWNLOAD";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_download_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_download_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "RENAME\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_download_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_new_folder_request", "[function]")
{
    SECTION("Function returns true for correct new folder request")
    {
        QByteArray message = "NEW FOLDER\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_new_folder_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "New Folder\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_new_folder_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of new folder request")
    {
        QByteArray message = "NEW FOLDER";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_new_folder_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_new_folder_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "RENAME\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_new_folder_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_cut_request", "[function]")
{
    SECTION("Function returns true for correct cut request")
    {
        QByteArray message = "CUT\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_cut_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "CuT\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_cut_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of cut request")
    {
        QByteArray message = "CUT";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_cut_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_cut_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "PASTE\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_cut_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_copy_request", "[function]")
{
    SECTION("Function returns true for correct copy request")
    {
        QByteArray message = "COPY\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_copy_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "CoPy\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_copy_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of copy request")
    {
        QByteArray message = "COPY";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_copy_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_copy_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "UPLOAD\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_copy_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_paste_request", "[function]")
{
    SECTION("Function returns true for correct paste request")
    {
        QByteArray message = "PASTE\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_paste_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "pASTE\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_paste_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of paste request")
    {
        QByteArray message = "PASTE";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_paste_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_paste_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "FILESYSTEM\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_paste_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_rename_request", "[function]")
{
    SECTION("Function returns true for correct rename request")
    {
        QByteArray message = "RENAME\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_rename_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "REnaME\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_rename_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of rename request")
    {
        QByteArray message = "RENAME";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_rename_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_rename_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "CLEAR\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_rename_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_delete_request", "[function]")
{
    SECTION("Function returns true for correct delete request")
    {
        QByteArray message = "DELETE\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_delete_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "DeLeTe\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_delete_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of delete request")
    {
        QByteArray message = "DELETE";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_delete_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_delete_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "LOGIN\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_delete_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_register_request", "[function]")
{
    SECTION("Function returns true for correct register request")
    {
        QByteArray message = "REGISTER\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_register_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "RegisteR\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_register_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of register request")
    {
        QByteArray message = "REGISTER";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_register_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_register_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "UPLOAD\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_register_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_login_request", "[function]")
{
    SECTION("Function returns true for correct login request")
    {
        QByteArray message = "LOGIN\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_login_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "LOgin\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_login_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of login request")
    {
        QByteArray message = "LOGIN";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_login_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_login_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "CUT\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_login_request(message);

        REQUIRE(result == output);
    }
}


TEST_CASE("is_clear_request", "[function]")
{
    SECTION("Function returns true for correct clear request")
    {
        QByteArray message = "CLEAR\r\n";
        TCPConnection connection;
        auto result = true;

        auto output = connection.is_clear_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong letter capitalization")
    {
        QByteArray message = "CleAR\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_clear_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for missing escape sequences at the end of clear request")
    {
        QByteArray message = "CLEAR";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_clear_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for empty request")
    {
        QByteArray message = "";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_clear_request(message);

        REQUIRE(result == output);
    }

    SECTION("Function returns false for wrong request")
    {
        QByteArray message = "REGISTER\r\n";
        TCPConnection connection;
        auto result = false;

        auto output = connection.is_clear_request(message);

        REQUIRE(result == output);
    }
}
